const http = require('http');

const port = 3000

const server = http.createServer(function (request, response) {
	
	if (request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Welcome to the login page.')
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port)

console.log(`Server is now accessible at localhost: ${port}`)